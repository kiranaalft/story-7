from django.apps import AppConfig


class S7AppConfig(AppConfig):
    name = 'S7App'
