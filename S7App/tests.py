from django.test import TestCase, Client
from django.urls import resolve
from .views import home, about, profile, contacts
from django.apps import apps
import unittest
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


    #Test Homepage
class S7UnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_S7_using_home_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
        
    def test_S7_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    #Test About Me Page
    def test_about_url_is_exist(self):
        response = Client().get('/about-me/')
        self.assertEqual(response.status_code,200)

    def test_S7_using_about_templates(self):
        response = Client().get('/about-me/')
        self.assertTemplateUsed(response, 'about.html')
        
    def test_S7_using_about_func(self):
        found = resolve('/about-me/')
        self.assertEqual(found.func, about)

    #Test Profile Page
    def test_profile_url_is_exist(self):
        response = Client().get('/my-profile/')
        self.assertEqual(response.status_code,200)

    def test_S7_using_profile_templates(self):
        response = Client().get('/my-profile/')
        self.assertTemplateUsed(response, 'profile.html')
        
    def test_S7_using_profile_func(self):
        found = resolve('/my-profile/')
        self.assertEqual(found.func, profile)

    #Test Contact Page    
    def test_contacts_url_is_exist(self):
        response = Client().get('/contacts/')
        self.assertEqual(response.status_code,200)

    def test_S7_using_contacts_templates(self):
        response = Client().get('/contacts/')
        self.assertTemplateUsed(response, 'contacts.html')
        
    def test_S7_using_contacts_func(self):
        found = resolve('/contacts/')
        self.assertEqual(found.func, contacts)

class S7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(S7FunctionalTest, self).setUp()
		
    def tearDown(self):
        self.selenium.quit()
        super(S7FunctionalTest, self).tearDown()

    def test_jquery(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/my-profile/')

        accordion = selenium.find_element_by_class_name('accordion')
        panel = selenium.find_element_by_class_name('panel')
        accordion.click()
        time.sleep(5)

        selenium = self.selenium
        selenium.get('http://localhost:8000/my-profile/')

        night = selenium.find_element_by_id('dark')
        night.click()
        time.sleep(5)

        day = selenium.find_element_by_id('light')
        day.click()
        time.sleep(5)
        self.tearDown()
