from django.urls import path
from . import views

app_name = 'S7App'

urlpatterns = [
    path('', views.home, name='home'),
    path('about-me/', views.about, name='about'),
    path('my-profile/', views.profile, name='profile'),
    path('contacts/', views.contacts, name='contacts'),
]